package uits.jv.test.mutilevel.domen.dao;

import uits.jv.test.mutilevel.domen.entity.Users;

/**
 *
 * @author Andriy Izmaylov
 */
public interface UsersDao extends BaseDao<Users, Integer> {

    Users getByUserNamne(String login);
}
