package uits.jv.test.mutilevel.domen.dao;

import uits.jv.test.mutilevel.domen.entity.Posts;

/**
 *
 * @author Andriy Izmaylov
 */
public interface PostsDao extends BaseDao<Posts, Integer>{
    
}
