package uits.jv.test.mutilevel.domen.dao;

import uits.jv.test.mutilevel.domen.entity.Comments;

/**
 *
 * @author Andriy Izmaylov
 */
public interface CommentsDao extends BaseDao<Comments, Integer>{
    
}
