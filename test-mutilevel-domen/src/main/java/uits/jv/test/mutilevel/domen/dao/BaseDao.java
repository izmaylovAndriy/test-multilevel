package uits.jv.test.mutilevel.domen.dao;

import java.util.List;

/**
 *
 * @author Andriy Izmaylov
 */
public interface BaseDao<T, I> {

    T getById(I key);

    T create(T o);

    boolean update(T o);

    boolean remove(T o);

    List<T> getList();
}
