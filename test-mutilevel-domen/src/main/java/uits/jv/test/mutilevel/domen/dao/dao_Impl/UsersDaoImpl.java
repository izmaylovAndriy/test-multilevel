package uits.jv.test.mutilevel.domen.dao.dao_Impl;

import uits.jv.test.mutilevel.domen.dao.UsersDao;
import uits.jv.test.mutilevel.domen.entity.Users;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import uits.jv.test.mutilevel.domen.HibernateUtil;
import uits.jv.test.mutilevel.domen.dao.AbstractDao;

public class UsersDaoImpl extends AbstractDao<Users, Integer> implements UsersDao {

    @Override
    public Users getByUserNamne(String login) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Users.class).add(Restrictions.eq("login", login));
        List<Users> users = criteria.list();
        return (users.size() > 0) ? users.get(0) : null;
    }

  
//
//    @Override
//    public Users getById(Serializable key) {
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        return (Users) session.get(Users.class, key);
//    }
//
//    @Override
//    public Users create(Users o) {
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = session.beginTransaction();
//        try {
//            session.save(o);
//            t.commit();
//        } catch (Exception e) {
//            t.rollback();
//            throw new RuntimeException("Cannot save user", e);
//        }
//
//        return o;
//    }
//
//    @Override
//    public boolean update(Users o) {
//        boolean updated = false;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = session.beginTransaction();
//        try {
//            session.merge(o);
//            t.commit();
//            updated = true;
//        } catch (Exception e) {
//            t.rollback();
//            throw new RuntimeException("Cannot update user", e);
//        }
//        return updated;
//    }
//
//    @Override
//    public boolean remove(Users o) {
//        boolean removed = false;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = session.beginTransaction();
//        try {
//            session.merge(o);
//            t.commit();
//            removed = true;
//        } catch (Exception e) {
//            t.rollback();
//            throw new RuntimeException("Cannot delete user", e);
//        }
//        return removed;
//    }
//
//    @Override
//    public List<Users> getList() {
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        return session.createCriteria(Users.class).list();
//    }

}
