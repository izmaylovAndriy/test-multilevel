package uits.jv.test.mutilevel.service;

import uits.jv.test.mutilevel.domen.dao.BaseDao;
import uits.jv.test.mutilevel.domen.dao.UsersDao;
import uits.jv.test.mutilevel.domen.dao.dao_Impl.UsersDaoImpl;
import uits.jv.test.mutilevel.domen.entity.Posts;
import uits.jv.test.mutilevel.domen.entity.Users;

/**
 *
 * @author Andriy Izmaylov
 */
public class UsersService {
     private UsersDao usersDao;
    BaseDao<Posts, Integer> posts;

    public UsersService() {
        usersDao = new UsersDaoImpl();
    }

    public UsersService(UsersDao usersDao, BaseDao<Posts, Integer> posts) {
        this.usersDao = usersDao;
        this.posts = posts;
    }
    
    Users getById(Integer id){
        return usersDao.getById(id);
    }
    
    
}
