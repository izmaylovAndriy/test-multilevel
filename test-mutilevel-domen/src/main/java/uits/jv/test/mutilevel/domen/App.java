package uits.jv.test.mutilevel.domen;

import uits.jv.test.mutilevel.domen.dao.dao_Impl.UsersDaoImpl;
import uits.jv.test.mutilevel.domen.dao.UsersDao;
import uits.jv.test.mutilevel.domen.entity.Users;

/**
 * 
 * @author Andriy Izmaylov
 */
public class App {

    public static void main(String[] args) {
        UsersDao uDao = new UsersDaoImpl();
//        System.out.println(uDao.getList());
//        Users user = new Users();
//        user.setFirstName("kunkka");
//        user.setLastName("Admiral");
//        uDao.create(user);
//        System.out.println(uDao.getList());
        Users vasya = uDao.getById(1);
        System.out.println(vasya);
        System.out.println("posts : ");
        System.out.println(vasya.getPostsList());
        HibernateUtil.getSessionFactory().close();
    }
}
