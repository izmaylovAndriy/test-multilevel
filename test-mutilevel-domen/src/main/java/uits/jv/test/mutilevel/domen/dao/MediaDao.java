package uits.jv.test.mutilevel.domen.dao;

import uits.jv.test.mutilevel.domen.entity.Media;

/**
 *
 * @author Andriy Izmaylov
 */
public interface MediaDao extends BaseDao<Media, Integer> {

}
