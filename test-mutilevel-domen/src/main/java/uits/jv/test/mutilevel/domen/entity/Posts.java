package uits.jv.test.mutilevel.domen.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Andriy Izmaylov
 */
@Entity
@Table(name = "posts")
@NamedQueries({
    @NamedQuery(name = "Posts.findAll", query = "SELECT p FROM Posts p")})
public class Posts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "post")
    private String post;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "post", fetch = FetchType.LAZY)
    private List<Comments> commentsList;
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users user;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "likedPostsList")
    private List<Users> usersLikedPostsList;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "post_media", joinColumns = {
        @JoinColumn(name = "id_post")}, inverseJoinColumns = {
        @JoinColumn(name = "id_media")})
    private List<Media> postMediaList;

    public Posts() {
    }

    public Posts(Integer id) {
        this.id = id;
    }

    public Posts(Integer id, String post) {
        this.id = id;
        this.post = post;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public List<Comments> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<Users> getUsersLikedPostsList() {
        return usersLikedPostsList;
    }

    public void setUsersLikedPostsList(List<Users> usersLikedPostsList) {
        this.usersLikedPostsList = usersLikedPostsList;
    }

    public List<Media> getPostMediaList() {
        return postMediaList;
    }

    public void setPostMediaList(List<Media> postMediaList) {
        this.postMediaList = postMediaList;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.post);
        hash = 89 * hash + Objects.hashCode(this.commentsList);
        hash = 89 * hash + Objects.hashCode(this.user);
        hash = 89 * hash + Objects.hashCode(this.usersLikedPostsList);
        hash = 89 * hash + Objects.hashCode(this.postMediaList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Posts other = (Posts) obj;
        if (!Objects.equals(this.post, other.post)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.commentsList, other.commentsList)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.usersLikedPostsList, other.usersLikedPostsList)) {
            return false;
        }
        if (!Objects.equals(this.postMediaList, other.postMediaList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Posts{" + "id=" + id + ", post=" + post + ", commentsList=" + commentsList + ", user=" + user + ", usersLikedPostsList=" + usersLikedPostsList + ", postMediaList=" + postMediaList + '}';
    }

}
