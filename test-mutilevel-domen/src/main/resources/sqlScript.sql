CREATE DATABASE IF NOT EXISTS `blog` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE blog;


DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`(
`id` int (10) unsigned NOT NULL AUTO_INCREMENT,
`first_name` varchar (32) NOT NULL,
`last_name` varchar (32) NOT NULL,
PRIMARY KEY(`id`)
);


DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts`(
`id` int(10) unsigned NOT NULL AUTO_INCREMENT ,
`id_user` int(10) unsigned NOT NULL ,
`post` varchar(255) NOT NULL,
PRIMARY KEY(`id`),
FOREIGN KEY (`id_user`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments`(
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_post` int (10) unsigned NOT NULL,
`comment` VARCHAR (255)  NOT NULL,
PRIMARY KEY(`id`),
FOREIGN KEY (`id_post`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media`(
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_post` int (10) unsigned NOT NULL,
`link` VARCHAR (255)  NOT NULL,
PRIMARY KEY(`id`),
FOREIGN KEY (`id_post`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `likes`(
`id_post` int(10) unsigned NOT NULL,
`id_user` int (10) unsigned NOT NULL,
FOREIGN KEY  (`id_post`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY  (`id_user`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
