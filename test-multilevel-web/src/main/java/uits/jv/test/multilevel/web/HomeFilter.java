package uits.jv.test.multilevel.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import uits.jv.test.mutilevel.domen.entity.Users;

/**
 *
 * @author Andriy Izmaylov
 */
@WebFilter(filterName = "HomeFilter", urlPatterns = {"/home"})
public class HomeFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();

        Object o = session.getAttribute("user");
        if (o == null && !(o instanceof Users)) {
            ((HttpServletResponse) response).sendRedirect("/test-multilevel-web/?notAuthorized=Please login");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
