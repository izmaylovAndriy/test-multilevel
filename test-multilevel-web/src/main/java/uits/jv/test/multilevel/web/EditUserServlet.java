package uits.jv.test.multilevel.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import uits.jv.test.mutilevel.domen.dao.dao_Impl.UsersDaoImpl;
import uits.jv.test.mutilevel.domen.entity.Users;

/**
 *
 * @author Andriy Izmaylov
 */
@WebServlet(name = "EditUserServlet", urlPatterns = {"/edit"})
public class EditUserServlet extends HttpServlet {
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        int userid;
//        try {
//            userid = Integer.parseInt(req.getParameter("id"));
//
//        } catch (NumberFormatException ex) {
//            resp.sendError(404, "Such user not found");
//            return;
//        }
//        Users user = new UsersDaoImpl().getById(Integer.SIZE);
//        if (user != null) {
//            HttpSession session = req.getSession();
//            Users user_ = (Users) session.getAttribute("user");
//            req.setAttribute("firstname", user_.getFirstname());
//            req.setAttribute("lastname", user_.getLastname());
//            req.setAttribute("user", user);
//            req.getRequestDispatcher("/WEB-INF/views/home/edit.jsp").forward(req, resp);
//        } else {
//            resp.sendError(404, "Such user not found");
//        }
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        int userid;
//        try {
//            userid = Integer.parseInt(req.getParameter("id"));
//
//        } catch (NumberFormatException ex) {
//            resp.sendError(404, "Such user not found");
//            return;
//        }
//        Users user = new UsersDaoImpl().getById(Integer.SIZE);
//        if (user != null) {
//            String firstname = req.getParameter("firstname");
//            String lastname = req.getParameter("lastname");
//            if (firstname == null || firstname.isEmpty()) {
//                resp.sendError(500, "empty firstname");
//                return;
//            }
//            if (lastname == null || lastname.isEmpty()) {
//                resp.sendError(500, "empty lastname");
//                return;
//            }
//            user.setFirstname(firstname);
//            user.setLastname(lastname);
//            new UsersDaoImpl().update(user);
//            resp.sendRedirect("/test-multilevel-web/home");
//
//        } else {
//            resp.sendError(404, "Such user not found");
//            return;
//        }
//    }
@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        int userid;
        
        try{
            userid = Integer.parseInt(req.getParameter("id"));
        } catch(NumberFormatException e){
            resp.sendError(404, "Such user not found");
            return;
        }
        
        
        Users user = new UsersDaoImpl().getById(userid);
        
        
        if (user != null){
            req.setAttribute("user", user);
            req.getRequestDispatcher("/WEB-INF/views/home/edit.jsp").forward(req, resp);
            
        } else {
            resp.sendError(404, "Such user not found");
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       int userid;
        
        try{
            userid = Integer.parseInt(req.getParameter("id"));
        } catch(NumberFormatException e){
            resp.sendError(404, "Such user not found");
            return;
        }
        
        Users user = new UsersDaoImpl().getById(userid);
        
        
        if (user != null){
            String firstname = req.getParameter("firstname");
             String lastname = req.getParameter("lastname");
             
             if (firstname == null || firstname.isEmpty()){
                 resp.sendError(500, "empty firstname");
                 return;
             }
             
             if (lastname == null || lastname.isEmpty()){
                 resp.sendError(500, "empty lastname");
                 return;
             }
            user.setFirstname(firstname);
            user.setLastname(lastname);
            new UsersDaoImpl().update(user);
            resp.sendRedirect("/test-multilevel-web/home");
            return;
            
        } else {
            resp.sendError(404, "Such user not found");
            return;
        }
        
    }
}
