package uits.jv.test.multilevel.web.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Universe
 */
public class StringValidator {

    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9]{4,20}@[a-zA-Z0-9]{2,10}\\.[a-zA-Z0-9]{2,4}$";
    private static final String PASSWORD_PATTERN = "^[[a-z][A-Z][0-9]]{3,12}$";

    public static boolean isEmpty(String data) {
        if (data == null) {
            return true;
        }
        data = data.trim();
//        if(data.length() == 0){
//            return true;
//        }
        return data.isEmpty();
    }

    public static boolean isEmailValid(String email) {
        return findMatches(EMAIL_PATTERN, email);
    }

    public static boolean isPasswordValid(String password) {
        return findMatches(PASSWORD_PATTERN, password);
    }

    private static boolean findMatches(String pattern, String value) {
        Pattern pat = Pattern.compile(pattern);
        Matcher matcher = pat.matcher(value);
        return matcher.find();
    }
}
