/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.test.multilevel.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import uits.jv.test.multilevel.web.util.StringValidator;
import uits.jv.test.mutilevel.domen.dao.dao_Impl.UsersDaoImpl;
import uits.jv.test.mutilevel.domen.entity.Users;

/**
 *
 * @author Andriy Izmaylov
 */
@WebServlet(name = "IndexServlet", urlPatterns = {"/index"})
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object o = req.getSession().getAttribute("user");
        if (o != null && o instanceof Users) {
            resp.sendRedirect("home");
        } else {
            String notAuthorized = req.getParameter("notAuthorized");
            if (notAuthorized != null) {
                req.setAttribute("errors", Arrays.asList(new String[]{notAuthorized}));
            }
            req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        List<String> errors = new ArrayList<>();

        if (StringValidator.isEmpty(login)) {
            errors.add("Login cannot be empty");
        } else {
            if (!StringValidator.isEmailValid(login)) {
                errors.add("Please specify valid login(email)");
            }
        }

        if (StringValidator.isEmpty(password)) {
            errors.add("Password cannot be empty");
        }

//        if (StringValidator.isEmailValid(login)) {
//            errors.add("Please specify valid login(email)");
//        }

        if (!errors.isEmpty()) {
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
        } else {
            Users user = new UsersDaoImpl().getByUserNamne(login);
            if (user != null) {
                if (user.getPassword().equals(password)) {
                    HttpSession session = req.getSession(true);
                    session.setAttribute("user", user);
                    resp.addCookie(new Cookie("sid", session.getId()));
                    resp.sendRedirect("home");

                } else {
                    errors.add("User not found");
                    req.setAttribute("erros", errors);
                    req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);

                }
            } else {
                errors.add("User not found");
                req.setAttribute("erros", errors);
                req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);

            }
        }
    }
    

//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String login = req.getParameter("login");
//        String password = req.getParameter("password");
//
//        List<String> errors = new ArrayList<>();
//
//        if (StringValidator.isEmpty(login)) {
//            errors.add("login cannot be empty");
//        } else {
//            if (StringValidator.isEmailValid(login)) {
//                errors.add("Please specify valid login (email)");
//            }
//        }
//
//        if (StringValidator.isEmpty(password)) {
//            errors.add("password cannot be empty");
//        }
//
////        if (StringValidator.isEmailValid(login)) {
////            errors.add("Please specify valid login (email)");
////        }
//        if (!errors.isEmpty()) {
//            req.setAttribute("erros", errors);
//            req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
//        } else {
//            Users user = new UsersDaoImpl().getByUserNamne(login);
//            if (user != null) {
//                if (user.getPassword().equals(password)) {
//                    HttpSession session = req.getSession(true);
//                    session.setAttribute("user", user);
//                    resp.addCookie(new Cookie("sid", session.getId()));
//                    resp.sendRedirect("home");
//                } else {
//                    errors.add("User not found");
//                    req.setAttribute("erros", errors);
//                    req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
//                }
//            } else {
//                errors.add("User not found");
//                req.setAttribute("erros", errors);
//                req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
//            }
//        }
//    }

}
