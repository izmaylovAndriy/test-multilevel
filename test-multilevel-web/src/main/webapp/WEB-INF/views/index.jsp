<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <form method="post" action="">
            <table>
                <tr>
                    <td colspan="2">
                        <c:if test="${errors != null}">
                            <ul>
                                <c:forEach var="error" items="${errors}">
                                    <li>${error}</li>
                                    </c:forEach>
                            </ul>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>Login</td>
                    <td>
                        <input type="email" name="login"/>
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="password"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" name="submit" value="Login"/>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>